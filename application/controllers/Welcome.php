<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->helper('date');
        $this->load->model('BlogModel');
	}
	
	public function index()
	{
		$data['data']=$this->BlogModel->display_records();
		$this->load->view('blog_index',$data);
	}
}