<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->helper('date');
        $this->load->model('BlogModel');
	}
	
	public function index()
    {
		$this->load->view('add_blog');
    }

	public function displaydata()
	{
		$data['data']=$this->BlogModel->display_records();
		$this->load->view('admin',$data);
	}

	public function delete()
    {
	  $id = $this->input->get('id');
      $this->db->where('id', $id);

      $this->db->delete('blog');

      $this->session->set_flashdata('success', 'Admin Deleted Successfully');

      redirect("AdminController/displaydata");
	}
	
	public function savingdata()
	{
		$data=$_POST;

		$a=$_FILES['image']['name'];

		$config = array(
		'upload_path'=>"upload",
				'allowed_types'=>"jpg|jpeg|png|gif",
				);
		$this->load->library('upload',$config);
		$this->upload->do_upload('image');
		$img=$this->upload->data();
		$data['image']= $a;

		$image_path = base_url("upload/".$a);
		$data['image'] = $a;
	
		$data = array(
			'title' => $this->input->post('title'),
			'domain'  => $this->input->post('domain'),
			'description' => $this->input->post('description'),
			'image' => $a,
			'created_at' => date('Y-m-d H:i:s')
		);
		
		if($this->db->insert('blog',$data))
		{
			redirect("AdminController/displaydata");
		}

		
	}
}