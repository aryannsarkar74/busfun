<?php
class BlogController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('date');
        $this->load->model('BlogModel');
    }

    public function index()
    {
        $id = $this->input->get('id');
		$blog_details['data'] = $this->BlogModel->find_blog($id);
		$this->blog_view($blog_details);
    }

    public function blog_view($blog_details)
    {
        $this->load->view('blog',$blog_details);
    }

    public function find_domain()
    {
        $domain = $this->input->get('domain');
		$domain_details['data'] = $this->BlogModel->find_domain($domain);
		$this->domain_view($domain_details);
    }

    public function domain_view($domain_details)
    {
        // print_r($domain_details);die;
        $this->load->view('domain_list',$domain_details);
    }
}