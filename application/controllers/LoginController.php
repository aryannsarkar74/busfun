<?php  
 defined('BASEPATH') OR exit('No direct script access allowed'); 

 class LoginController extends CI_Controller {  
      function __construct(){
		parent::__construct();
		error_reporting(0);	
	}
     public function index()
     {
          $this->load->view('admin_login');
     } 

      function login_validation()  
      {  
          
           $this->form_validation->set_rules('username', 'username', 'required');  
           $this->form_validation->set_rules('password', 'Password', 'required|max_length[12]');  
           if($this->form_validation->run())  
           {  
                //true  
                $username = $this->input->post('username');  
                $password = $this->input->post('password');  
                
                //model function  
                $this->load->model('AuthModel');  

                if($this->AuthModel->can_login($username, $password))  
                {  
                    $info = $this->AuthModel->current_admin($username);

                     $session_data = array(  
                          'id'     =>     $info->id,
                          'username'     =>     $info->username,
                     );

                     $this->session->set_userdata($session_data);  
                     redirect(base_url() . 'index.php/LoginController/enter');  
                }  
                else  
                {  
                     $this->session->set_flashdata('error', 'Invalid Email or Password');  
                     redirect(base_url());  
                }  
           }  
           else  
           {  
                //false  
                $this->login();  
           }  
      }


      function enter()
      {  
           if($this->session->userdata('username') != '')  
           {  
               redirect(base_url() . 'index.php/AdminController/displaydata');  
           }  
           else  
           {  
                redirect(base_url() . 'LoginController/login');  
           }  
      }


      function logout()  
      {  
          $this->session->sess_destroy();
          $this->session->unset_userdata('username');  
          redirect(base_url());  
      }  
}