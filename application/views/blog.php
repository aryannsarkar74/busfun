<?php include 'header.php' ?>

		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<div class="container pb50">
    	<div class="row">
        	<div class="col-md-9 mb40">
            <article>
				<br><br>
                <img style="height:200px;width:200px" src="../upload/<?php echo $data->image; ?>" alt="" class="img-fluid mb30">
                <div class="post-content">
					<br>
					<h3><?php echo $data->title; ?></h3>

					<ul class="post-meta list-inline">
						<li class="list-inline-item">
							<i class="fa fa-tags"></i>
							<a href="<?=base_url()?>index.php/BlogController/find_domain?domain=<?php echo $data->domain;?>">
								<?php echo $data->domain; ?>
							</a>
                        </li>
                    </ul>
                    
                    <p class="lead"><?php echo $data->description; ?></p>
                    
                    <hr class="mb40">
				</div>
            </article>
            <!-- post article-->

        </div>
        
    </div>
</div>
		
<?php include 'footer.php' ?>