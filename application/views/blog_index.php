<?php include 'header.php' ?>


		<section class="news-section">
			<div class="container">
				<div class="title-section">
					<div class="left-part">
						<span>Blog</span>
						<h1>Latest News</h1>
					</div>
				
				</div>
				<div class="news-box">
					<div class="row">

					<?php
					$i=1;
					foreach($data as $row)
					{ ?>
						<div class="col-lg-3 col-md-6">
							<div class="blog-post">
								<a href="<?php echo base_url();?>index.php/BlogController?id=<?php echo $row->id;?>">
									<img style="height:250px;width:250px" src="upload/<?php echo $row->image; ?>" alt="">
								</a>
								<div class="post-content">
									<a class="category" href="index.php/BlogController/find_domain?domain=<?php echo $row->domain;?>">
									<?php echo $row->domain; ?></a>
									<h2><a href="index.php/BlogController?id=<?php echo $row->id;?>"><?php echo $row->title; ?></a></h2>
									<div class="post-meta date">
										<i class="material-icons">access_time</i> 
										<?php $created_at1 = date("d-m-Y", strtotime($row->created_at));?>
										<?php echo $created_at1; ?>
									</div>
								</div>
							</div>
						</div>
					<?php
					$i++;
					}
					?>
					</div>
				</div>
			</div>
		</section>
		<!-- End news section -->

		<!-- testimonial-section 
			================================================== -->
		<section class="testimonial-section">
			<div class="container">
				<div class="testimonial-box owl-wrapper">
					
					<div class="owl-carousel" data-num="1">
					
						<div class="item">
							<div class="testimonial-post">
								<p> “Design-driven, customized and reliable solution for your token development and management system to automate sales processes.”</p>
								<div class="profile-test">
									<div class="avatar-holder">
										<img src="upload/testimonials/testimonial-avatar-1.jpg" alt="">
									</div>
									<div class="profile-data">
										<h2>Nicole Alatorre</h2>
										<p>Designer</p>
									</div>
								</div>
							</div>
						</div>
					
						<div class="item">
							<div class="testimonial-post">
								<p> “Design-driven, customized and reliable solution for your token development and management system to automate sales processes.”</p>
								<div class="profile-test">
									<div class="avatar-holder">
										<img src="upload/testimonials/testimonial-avatar-2.jpg" alt="">
									</div>
									<div class="profile-data">
										<h2>Nicole Alatorre</h2>
										<p>Designer</p>
									</div>
								</div>
							</div>
						</div>
					
						<div class="item">
							<div class="testimonial-post">
								<p> “Design-driven, customized and reliable solution for your token development and management system to automate sales processes.”</p>
								<div class="profile-test">
									<div class="avatar-holder">
										<img src="upload/testimonials/testimonial-avatar-3.jpg" alt="">
									</div>
									<div class="profile-data">
										<h2>Nicole Alatorre</h2>
										<p>Designer</p>
									</div>
								</div>
							</div>
						</div>
					
						<div class="item">
							<div class="testimonial-post">
								<p> “Design-driven, customized and reliable solution for your token development and management system to automate sales processes.”</p>
								<div class="profile-test">
									<div class="avatar-holder">
										<img src="upload/testimonials/testimonial-avatar-4.jpg" alt="">
									</div>
									<div class="profile-data">
										<h2>Nicole Alatorre</h2>
										<p>Designer</p>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
		<!-- End testimonial section -->

	
		<?php include 'footer.php' ?>