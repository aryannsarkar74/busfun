<?php include 'header.php' ;
if ($this->session->userdata('username') == FALSE) 
{
    redirect(base_url());  
}?>
<br><br>
<section>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-8 col-xl-6">
          <div class="row">
            <div class="col text-center">
              <h1>Add Blog</h1>
              <p class="text-h3">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia. </p>
            </div>
          </div>
          <?= form_open_multipart('AdminController/savingdata') ?>
          <div class="row align-items-center">
            <div class="col mt-4">
              <input type="text" name="title" class="form-control" placeholder="Blog Title">
            </div>
          </div>
          <div class="row align-items-center mt-4">
            <div class="col">
                <select class="form-control" name="domain">
                    <option>Domain</option>
                    <option value="Education">Education</option>
                    <option value="Academics">Academics</option>
                    <option value="Sports">Sports</option>
                    <option value="Finance">Finance</option>
                </select>
            </div>
          </div>
          <div class="row align-items-center mt-4">
          <h6>&nbsp;&nbsp;&nbsp;&nbsp;Add Image</h6>
                <div class="col">
                <input
                    type="file"
                    name="image"
                    class="drop-zone__input"
                    value="<?=set_value('image')?>"
                    
                    />
                </div>
            </div>
          <div class="row align-items-center mt-4">
            <div class="col">
              <textarea cols="8"
                rows="5"
                name="description"
                maxlength="240" type="text"
                class="form-control" placeholder="Blog Description"></textarea>
            </div>
           
          </div>
          <div class="row justify-content-start mt-4">
            <div class="col">
              <input type="submit" value="Submit" class="btn btn-primary mt-4">
            </div>
          </div>
        </div>
        <form>
      </div>
    </div>
  </section>
  <br><br>
  <?php include 'footer.php' ?>