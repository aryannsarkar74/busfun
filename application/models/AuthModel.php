<?php  
 class AuthModel extends CI_Model  
 {  
     function can_login($username, $password)  
     {  
          $this->db->where('username', $username);  
          $this->db->where('password', $password);  
          $query = $this->db->get('admin');  
          
          if($query->num_rows() > 0)  
          {  
               return true;  
          }  
          else  
          {  
               return false;       
          }  
     }

     public function current_admin($username)
     {
          $query1 = $this->db->select('*')->from('admin')->where('username',$username)->get();

          return $query1->row();
     }
}