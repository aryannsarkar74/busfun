<?php


class BlogModel extends CI_Model
{
    function display_records()
    {
        $this->db->select('*');
        $this->db->from('blog');

        $query = $this->db->get();  

        return $query->result();
    }

    public function find_blog($id)
    {
        $q = $this->db->where('id',$id)->get('blog');

        return $q->row();
    }

    public function find_domain($domain)
    {
        $this->db->select('*');
        $this->db->from('blog');
        $this->db->where('domain',$domain);

        $query = $this->db->get();  

        return $query->result();
    }
}
